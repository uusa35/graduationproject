@extends('frontend.layouts._one_col')

@section('content')
@if($page)
    <article>
        <div class="panel panel-default">
            <div class="panel-heading">

                <div class="row">
                    <div class="col-lg-9"><h3>{{ $page->header }}</h3>
                    <p class="pull-left text-muted text-center">
                        <i class="fa fa-sm fa-clock-o"></i> Date : {{  $page->created_at->format ('Y-m-d') }}
                    </p>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-lg-10">
                    <p style="text-align: justify;">
                        {!! $page->body !!}
                    </p>
                </div>
                <div class="col-lg-2">
                    <img class="col-lg-12 pull-right img-responsive thumbnail" src="/img/pages/large/{{ $page->image_first }}" alt=""/>

                    <img class="col-lg-12 pull-left img-responsive thumbnail" src="/img/pages/large/{{ $page->image_second }}" alt=""/>
                </div>
            </div>
        </div>
    </article>
@else
    <h1 class="well">There are no items in the database.</h1>
@endif
@endsection