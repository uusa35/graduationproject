@extends('frontend.layouts.master')


@section('container')

    <div class="row">

        @yield('content')

    </div>

@endsection


