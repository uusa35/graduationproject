<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="nl"><![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="nl"><![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="nl"><![endif]-->
<!--[if IE]>
<html class="no-js ie" lang="nl"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js" lang="nl"><!--<![endif]-->

<head>

    <meta charset="utf-8">
    <title>@yield(strip_tags('title'),'Graduation Project')</title>
    <meta name="description" content="My Graduation Project">
    <meta name="author" content="MEME">
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

    @section('styles')
        @include('frontend.styles.styles')
    @show

</head>

<body>

@include('frontend.partials.nav')

<div class="container">

    <div class="content">


    @include('frontend.partials.notifications')

    @section('container')
    @show

    </div>
</div>

@section('scripts')
    @include('frontend.scripts.scripts')
@show


</body>


</html>
