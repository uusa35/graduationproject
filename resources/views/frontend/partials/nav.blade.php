
<nav class="navbar navbar-inverse navbar-fixed-top col-lg-12">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{ route('home') }}">Home</a></li>
                @if(count($menu) > 0  && $menu != 'menu')
                    @foreach($menu as $menu)
                        <li><a href="{{ route('page',$menu->id) }}">{{$menu->title}}</a></li>
                    @endforeach
                @endif
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Control Panel <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        @if(Auth::user())
                        <li><a href="#" class="text-warning">Logged in as {{ Auth::user()->name }}</a></li>
                            @if(Auth::user()->getCurrentUserRole())
                                <li role="separator" class="divider"></li>
                                <li class="text-danger"><a href="{{ route('adminzone.index') }}"><i class="fa fa-user fa-aw"></i> DashBoard </a></li>
                            <li role="separator" class="divider"></li>
                            <li class="text-danger"><a href="{{ route('adminzone.create') }}"><i class="fa fa-pencil-square fa-aw"></i> Create New Page</a></li>
                            @endif
                        <li role="separator" class="divider"></li>
                        <li class="bg-danger"><a href="/auth/logout"><i class="fa fa-fw fa-sign-out"></i>Log out</a></li>
                        @else
                        <li><a href="/auth/login" class="text-warning">Log in</a></li>
                        <li><a href="/auth/register" class="text-warning">Regsiter</a></li>
                        @endif
                    </ul>
                </li>
            </ul>

        </div>
        <!--/.nav-collapse -->
    </div>
</nav>

