@extends('backend.layouts._one_col')

@section('content')
    <div class="row">
        <div class="col-xs-12">

            <!-- START CONTENT ITEM -->
            <ul class="nav nav-tabs">
                <li id="tab-1" class="active" href="#step1"><a class="active" href="#step1" data-toggle="tab"><i
                                class="fa fa-aw fa-book"></i>&nbsp;{{ trans('word.pages') }} </a></li>
            </ul>

            {{--All Books--}}

            <div class="tab-content">

                <div class="tab-pane active" id="step1">
                    <div class="row">
                        <div class="col-xs-12 paddingTop10">
                            @if($allPages->count() > 0)
                                <table class="table table-striped table-order" id="booksTable">
                                    <thead>
                                    <tr style="background-color:#E0E0E0; text-align: center;">
                                        <th class="hidden-xs">{{ trans('word.id') }}</th>
                                        <th>{{ trans('word.title') }}</th>
                                        <th>{{ trans('word.header') }}</th>
                                        <th>{{ trans('word.body') }}</th>
                                        <th>{{ trans('word.created-at') }}</th>
                                        <th>{{ trans('word.edit') }}</th>
                                        <th>{{ trans('word.remove') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($allPages as $page)
                                        <tr>
                                            <td class="hidden-xs">{{ $page->id }}</td>
                                            <td>
                                                <a href="{{ action('HomeController@show', $page->id) }}"> {!!
                                                    $page->title
                                                    !!} </a>


                                            </td>
                                            <td>
                                                <p> {!! Str::words(strip_tags($page->header),4) !!} </p>

                                            </td>
                                            <td>
                                                <p> {!! Str::words(strip_tags($page->body),4) !!} </p>
                                            </td>
                                            <td>
                                                <span> {{ $page->created_at->format('Y-m-d') }} </span>
                                            </td>
                                            <td class="text-left">
                                                <a class="btn btn-sm btn-info text-center" href="{{ route('adminzone.edit',$page->id) }}"><i class="fa fa-edit fa-fw"></i></a>
                                            </td>
                                            <td class="text-left">
                                                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal"
                                                        data-target="#myModal-{{$page->id}}" href="">
                                                    <i class="fa fa-trash-o fa-2x"></i>
                                                </button>
                                                @include('backend.partials._delete_modal')
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="alert alert-warning"
                                     role="alert">{{ trans('word.no-pages-found') }}</div>
                            @endif
                        </div>
                        {!! $allPages->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection