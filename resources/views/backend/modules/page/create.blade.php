@extends('frontend.layouts._one_col')



@section('scripts')
    @parent
    {{--<script src="/css/bower_components/tinymce/tinymce.min.js"></script>
    <script src="/css/bower_components/tinymce/tinymce.jquery.min.js"></script>
    <script src="/css/bower_components/tinymce/plugins/template/plugin.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea.editor",
            plugins: [
                ["advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker"],
                ["searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking"],
                ["save table contextmenu directionality emoticons template paste textcolor  directionality jbimages"]
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages | print preview media fullpage | forecolor backcolor emoticons | ltr rtl ",
            relative_urls: true
        });
    </script>--}}
@stop



@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-lg-6">
                    <h3>{{ trans('word.book-create') }}</h3>
                </div>
                <div class="col-lg-6">
                    <p style="color:red;">(*) - {{ trans('word.fields-required') }}</p>

                    <p style="color:red;">(*) - {{ trans('word.cover-instructions') }}</p>
                </div>
            </div>
        </div>
        <div class="panel-body">
            {!! Form::open(['route' => 'adminzone.store', 'method' => 'post','files'=>'true'],
            ['class'=>'form-horizontal']) !!}
            <div class="row">
                <div class="form-group col-md-3 col-lg-3">
                    {!! Form::label('image_first', trans('word.image') , ['class' => 'control-label']) !!}*
                    {!! Form::file('image_first', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-3 col-lg-3">
                    {!! Form::label('image_second', trans('word.image') , ['class' => 'control-label']) !!}*
                    {!! Form::file('image_second',['class' => 'form-control' ]) !!}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4 col-lg-4">
                    {!! Form::label('title', trans('word.page-title'), ['class' => 'control-label']) !!}*
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group col-md-4 col-lg-4">
                    {!! Form::label('header', trans('word.title'), ['class' => 'control-label']) !!}*
                    {!! Form::text('header', null, ['class' => 'form-control']) !!}
                </div>

            </div>
            <div class="form-group">
                {!! Form::label('body', trans('word.content'), ['class' => 'control-label']) !!}*
                {!! Form::textarea('body', null, ['class' => 'form-control editor']) !!}
            </div>

            <div class="form-group">
                <div class="col-lg-6">
                    {!! Form::submit(trans('word.save'), ['class' => 'btn btn-primary form-control']) !!}
                </div>
                <div class="col-lg-6">
                    <a class="btn btn-danger form-control" href="{{ URL::previous() }}">{{ trans('word.cancel') }}</a>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}

@endsection