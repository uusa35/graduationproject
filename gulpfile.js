var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss')

    mix.styles([
     //"../../bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css",
        //"css/custom.css",
    ], 'public/css/everything.css');

    mix.scripts([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js',
    ], 'public/js/app.js', 'resources/', './');

    mix.version([
     'public/css/app.css',
     'public/js/app.js'
    ]);

});
