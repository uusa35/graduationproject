<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    private $tables = [
        'users',
        'password_resets',
        'roles',
        'user_role',
        'pages'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        if (\Illuminate\Support\Facades\App::environment() === 'local') {

            Model::unguard();

            $this->cleanDatabase();

            $this->call('UserTableSeeder');
            $this->call('RoleTableSeeder');
            $this->call('PageTableSeeder');

            Model::reguard();
        }


    }

    private function cleanDatabase()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        foreach ($this->tables as $table) {
            DB::table($table)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
