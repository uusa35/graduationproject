<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define('App\Src\User\User', function ($faker) {
    return [
        'name' => $faker->name,
        'email' => 'uusa35@gmail.com',
        'password' => \Illuminate\Support\Facades\Hash::make('admin'),
        'remember_token' => str_random(10),
    ];
});

$factory->define('App\Src\Role\Role', function ($faker) {
    return [
        'name' => $faker->randomElement(['Admin', 'Admin'])
    ];
});

$factory->define('App\Src\Page\Page',function ($faker) {
    return [
        'title' => $faker->word(2),
        'header' => $faker->sentence(1),
        'body' => $faker->sentence(15),
        'image_first' => 'http://placehold.it/150x150',
        'image_second' => 'http://placehold.it/150x150',
    ];
});

for ($i = 0; $i <= 1; $i++) {
    DB::table('user_role')->insert([
        'user_id' => '1',
        'role_id' => rand(1, 2)
    ]);
}