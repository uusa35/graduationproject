<?php

namespace App\Src\Page;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';
    protected $fillable = ['title','header','body','image_first','image_second'];
}
