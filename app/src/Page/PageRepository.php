<?php
/**
 * Created by PhpStorm.
 * User: usamaahmed
 * Date: 7/24/15
 * Time: 6:58 PM
 */

namespace App\Src\Page;


use App\Core\BaseRepository;

class PageRepository extends BaseRepository {


    public function __construct(Page $page) {
        $this->model = $page;
    }


}