<?php
/**
 * Created by PhpStorm.
 * User: usamaahmed
 * Date: 7/23/15
 * Time: 11:51 PM
 */

namespace App\Src\User;


trait UserHelpers {


    public function getCurrentUserRole() {

        return $this->roles()->first()->name;
    }
}