<?php
/**
 * Created by PhpStorm.
 * User: usamaahmed
 * Date: 7/23/15
 * Time: 5:54 PM
 */

namespace App\Src\User;


use App\core\BaseRepository;

class UserRepository extends BaseRepository
{

    public function __construct(User $user)
    {
        $this->model = $user;
    }

}