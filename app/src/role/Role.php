<?php

namespace App\Src\Role;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //

    protected $table = 'roles';

    public function users() {

        return $this->belongsToMany('App\Src\User\User','user_role');

    }
}
