<?php

namespace App\Jobs;

use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

class CreateImage extends Job implements SelfHandling
{
    public $model;
    public $request;


    /**
     * Create a new job instance.
     *
     * @param Book $book
     * @param $request
     */
    public function __construct(Model $model, $request)
    {
        $this->model = $model;

        $this->request = $request;

    }

    /**
     * Execute the job.
     *
     * @param Image $cover
     */
    public function handle(Image $cover)
    {
        // check the array of covers for cover_ar and cover_en
        // creating array
        // creating the covers
        $covers = [];

        if ($this->request->hasFile('image_first')) {

            $covers[] ='image_first';

        }

        if ($this->request->hasFile('image_second')) {

            $covers[] = 'image_second';

        }

        //$covers = ['cover_ar','cover_en'];
        $this->createCovers($covers, $cover);

    }

    public function CreateCovers($covers,$cover) {

        foreach ($covers as $coverImage) {

            $fileName = $this->request->file($coverImage)->getClientOriginalName();

            $fileName = Str::random(5) . '' . $fileName;

            $realPath = $this->request->file($coverImage)->getRealPath();

            $cover->make($realPath)->resize('195',

                '277')->save(public_path('img/pages/thumbnail/' . $fileName));

            $cover->make($realPath)->resize('360',
                '510')->save(public_path('img/pages/large/' . $fileName));

            $this->model->update([$coverImage => $fileName]);

            $this->model->save();

        }

    }
}
