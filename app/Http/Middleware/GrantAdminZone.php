<?php

namespace App\Http\Middleware;

use Closure;

class GrantAdminZone
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->getCurrentUserRole() === 'Admin') {

            return $next($request);

        } else {

            return redirect('/')->with(['error' => trans('word.needs-admin-auth')]);

        }
    }
}
