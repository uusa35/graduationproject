<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/***************************************************************************************************
 * Authentication
 ***************************************************************************************************/
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

/***************************************************************************************************
 * non authenticated - guests routes
 ***************************************************************************************************/
Route::get('/',['as'=>'home','uses'=>'HomeController@index']);
Route::get('/home',['as'=>'home','uses'=>'HomeController@index']);
Route::get('page/{id}',['as'=>'page','uses'=>'HomeController@show']);


/***************************************************************************************************
 * Admin Routes
 ***************************************************************************************************/
Route::group(['middleware'=>'admin.zone'], function () {


    Route::group(['namespace' => 'Admin'],function () {


        Route::resource('adminzone','DashBoardController');

    });

});


/***************************************************************************************************
 * Lessons learnt
 ***************************************************************************************************/