<?php

namespace App\Http\Controllers;

use App\Src\Page\PageRepository;
use Illuminate\Http\Request;
use App\Http\Requests;


class HomeController extends Controller
{
    public $pageRepository;

    public function __construct(PageRepository $pageRepository) {

        $this->pageRepository = $pageRepository;

    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $pageIndex = $this->pageRepository->model->first();

        return view('frontend.pages.template',['page'=>$pageIndex]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $pageIndex = $this->pageRepository->findWhereId($id);

        return view('frontend.pages.template',['page'=>$pageIndex]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
