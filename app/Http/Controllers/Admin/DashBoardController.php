<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\CreateImage;
use App\Src\Page\PageRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashBoardController extends Controller
{

    public $pageRepository;

    public function  __construct(PageRepository $pageRepository)
    {

        $this->pageRepository = $pageRepository;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $allPages = $this->pageRepository->model->paginate(8);

        return view('backend.modules.page.index', ['allPages' => $allPages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.modules.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Requests\CreatePage $request)
    {
        $pageCreated = $this->pageRepository->model->create(

            $request->except('_token', 'image_first', 'image_second')

        );

        $imagesCreated = $this->dispatch(new CreateImage($pageCreated, $request));

        if ($pageCreated) {

            return redirect()->back()->with(['success' => 'success-page-created']);
        }

        return redirect()->back()->with(['error' => 'error-page-created']);


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $page = $this->pageRepository->findWhereId($id);
        return view('backend.modules.page.edit', ['page' => $page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

        if (!$request->hasFile('image_first')) {

            $request->except('image_first');

        }
        if (!$request->hasFile('image_second')) {

            $request->except('image_second');

        }
        $page = $this->pageRepository->findWhereId($id);

        // update the book table
        $page->fill($request->except('_token', '_method', 'pageId'));

        $hasBodyChanged = $page->isDirty('body') ? true : false;

        try {

            $page->update($request->except('_token', '_method', 'pageId'));

        } catch (\Exception $e) {

            return redirect()->back()->with('error', trans('word.error-update'));
        }

        $this->dispatch(new CreateImage($page,$request));

        return redirect()->back()->with('success', trans('word.success-update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($id === 1) {
            return redirect()->back()->with('error', trans('word.error-delete'));
        } elseif ($this->pageRepository->findWhereId($id)->delete()) {

            return redirect()->back()->with('success', trans('word.success-delete'));
        }
        return redirect()->back()->with('error', trans('word.error-delete'));

    }
}
