<?php
/**
 * Created by PhpStorm.
 * User: usamaahmed
 * Date: 7/23/15
 * Time: 2:52 PM
 */

namespace App\Core;

use Illuminate\Database\Eloquent\Model;

class BaseRepository extends Model {

    public $model;

    public function findAll() {
        return $this->model->all();
    }

    public function findWhereId($id) {
        return $this->model->where('id','=',$id)->first();
    }

}