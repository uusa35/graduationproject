<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {


        $menu = DB::table('pages')->select('id', 'title')->get();

        if ($menu) {
            // share the contact us information all over the views from the cache
            view()->share(['menu' => $menu]);
        }
        else {
            view()->share(['menu' => 'menu']);
        }



    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
